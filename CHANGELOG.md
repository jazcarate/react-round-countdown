# Changelog

## 0.1.1
- Added this changelog
- Added style to the Countdown

## 0.2.0
- Make the configurations additive


## 1.0.0
- Properly compute ever (roughly) 1 second
- Calculate duration based on actual time passed, and not decrementing a variable
- Update license year
- Change how to start a timer