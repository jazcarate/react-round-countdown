# React Round Countdown
React component to render a countdown (that goes to the negative numbers)

## Install
Using `npm`: 
```bash
npm install react-round-countdown --save
```

Using `yarn`:
```bash
yarn add react-round-countdown
```

## Motivation
I needed a countdown for a [Magic: the Gathering](https://en.wikipedia.org/wiki/Magic:_The_Gathering) tournament, in an offline setting. So I decided to create my own countdown.

## Usage
### Example
There is a `create-react-app` app in the `example/` folder you can check out.

### In the code
A simple use case of a 50 minute timer:
```js
import React from 'react';
import ReactDOM from 'react-dom';
import Countdown from 'react-round-countdown';

ReactDOM.render(
  <Countdown end={Date.now() + 50 * 60* 1000} />,
  document.getElementById('root')
);
```

## Docs
This module exports just the one component `<Countdown>`.

### Props
| prop   | description                                                          | type                     | default                                                     |
| ------ | -------------------------------------------------------------------- | ------------------------ | ----------------------------------------------------------- |
| end    | The end date                                                         | `number | string | Date` | --                                                          |
| onTick | _[optional]_ a callback on each tick with the remaining milliseconds | `(n: number) => void`    | --                                                          |
| colors | _[optional]_ the colors to represent 3 different stages              | `ColorConfig`            | `{ normal: 'black', almostOut: 'orange', negative: 'red' }` |
| style  | _[optional]_ Style the countdown component                           | `CSSProperties`          | --                                                          |

## License
MIT

