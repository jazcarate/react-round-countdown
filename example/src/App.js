import React, { useState } from 'react';
import './App.css';
import Countdown from 'react-round-countdown';

function App() {
  const [end, setEnd] = useState(Date.now() + 10001);

  return (
    <div className="App">
      <header className="App-counter">
        <Countdown end={end} style={{ fontSize: '6em' }} />

        <button
          className="App-button"
          onClick={() => setEnd(e => e + 5000)}>
          Add 5 seconds
        </button>
      </header>
    </div>
  );
}

export default App;
