import React, { FunctionComponent, useEffect, useState, CSSProperties } from 'react';
import { renderDuration } from './renderDuration';
import color, { ColorConfig } from './color';
import { clearProperInterval, setProperInterval } from './properInterval';
import { toTime, ExtendedTime } from './time';

export type CountdownProps = {
  onTick?: (n: number) => void;
  colors?: Partial<ColorConfig>;
  style?: CSSProperties;
  end: ExtendedTime;
};

function noop() {}

const Countdown: FunctionComponent<CountdownProps> = ({
  end,
  onTick = noop,
  colors = {},
  style = {},
}) => {
  const realEnd = toTime(end);

  function duration() {
    return realEnd - Date.now();
  }

  const [remaining, setRemaining] = useState(duration());

  useEffect(() => {
    setRemaining(duration());
    const interval = setProperInterval(() => {
      const newVal = duration();
      setRemaining(newVal);
      onTick(newVal);
    }, 1000);

    return () => clearProperInterval(interval);
  }, [end]);

  return (
    <>
      <p
        style={{
          color: color(remaining, colors),
          ...style,
        }}
      >
        {renderDuration(remaining)}
      </p>
    </>
  );
};

export default Countdown;
