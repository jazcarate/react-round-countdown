import color, { ColorConfig, defaultColorConfig } from './color';

const MINUTES = 60 * 1000;

describe('#color', () => {
  describe('default config', () => {
    describe('when there is more than 5 minutes remaining', () => {
      it('is black', () => {
        expect(color(10 * MINUTES)).toEqual('black');
      });
    });

    describe('when there is less than 5 minutes remaining', () => {
      it('is orange', () => {
        expect(color(3 * MINUTES)).toEqual('orange');
      });
    });

    describe('when the time is up', () => {
      it('is red', () => {
        expect(color(-0.1)).toEqual('red');
      });
    });
  });

  describe('custom config', () => {
    const config: ColorConfig = {
      almostOut: '#FAFAFA',
      negative: '#CACACA',
      normal: '#BEBEBE',
    };
    describe('when there is more than 5 minutes remaining', () => {
      it('is black', () => {
        expect(color(10 * MINUTES, config)).toEqual(config.normal);
      });
    });

    describe('when there is less than 5 minutes remaining', () => {
      it('is orange', () => {
        expect(color(3 * MINUTES, config)).toEqual(config.almostOut);
      });
    });

    describe('when the time is up', () => {
      it('is red', () => {
        expect(color(-0.1, config)).toEqual(config.negative);
      });
    });
  });

  describe('partial config', () => {
    const config = {
      normal: '#BEBEBE',
    };

    describe('when there is more than 5 minutes remaining', () => {
      it('is the configured normal color', () => {
        expect(color(10 * MINUTES, config)).toEqual(config.normal);
      });
    });

    describe('when there is less than 5 minutes remaining', () => {
      it('is the default color', () => {
        expect(color(3 * MINUTES, config)).toEqual(defaultColorConfig.almostOut);
      });
    });

    describe('when the time is up', () => {
      it('is the default color', () => {
        expect(color(-0.1, config)).toEqual(defaultColorConfig.negative);
      });
    });
  });
});
