import { ColorProperty } from 'csstype';

export type ColorConfig = {
  normal: ColorProperty;
  almostOut: ColorProperty;
  negative: ColorProperty;
};

export const defaultColorConfig: ColorConfig = {
  normal: 'black',
  almostOut: 'orange',
  negative: 'red',
};

const FIVE_MINUTES = 300000;

export default function color(remaining: number, config?: Partial<ColorConfig>): ColorProperty {
  const realConfig = { ...defaultColorConfig, ...config };

  if (remaining > FIVE_MINUTES) return realConfig.normal;
  if (remaining > 0) return realConfig.almostOut;
  return realConfig.negative;
}
