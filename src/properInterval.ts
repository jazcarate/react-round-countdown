// Based loosely on https://gist.github.com/jakearchibald/cb03f15670817001b1157e62a076fe95

import { Time } from './time';

interface Interval {
  stop: () => void;
}

export function setProperInterval(callback: (time: Time) => void, ms: number): Interval {
  let playing = true;
  const start = document.timeline.currentTime || Date.now();

  function frame(time: Time) {
    if (!playing) return;
    callback(time);
    scheduleFrame(time);
  }

  function scheduleFrame(time: Time) {
    const elapsed = time - start;
    const roundedElapsed = Math.round(elapsed / ms) * ms;
    const targetNext = start + roundedElapsed + ms;
    const delay = targetNext - performance.now();
    setTimeout(() => requestAnimationFrame(frame), delay);
  }

  scheduleFrame(start);
  return { stop: () => (playing = false) };
}

export function clearProperInterval(interval: Interval) {
  interval.stop();
}
