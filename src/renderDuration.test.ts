import { renderDuration, minute, second } from './renderDuration';

describe('#renderDuration', () => {
  describe('when de duration is less than a seconds', () => {
    it('renders as "00:00"', () => {
      expect(renderDuration(0.4)).toEqual('00:00');
    });
  });

  describe('when de duration is less than an hour', () => {
    it('renders as "hh:mm"', () => {
      expect(renderDuration(30 * minute + 10 * second)).toEqual('30:10');
    });
  });

  describe('when de duration is less than a minute', () => {
    it('pads the minutes with a "0"', () => {
      expect(renderDuration(10 * second)).toEqual('00:10');
    });
  });

  describe('when de duration is less than ten minutes', () => {
    it('pads the minutes with a "0"', () => {
      expect(renderDuration(3 * minute + 10 * second)).toEqual(
        expect.stringMatching(/^0[0-9]:[0-9]{2}$/)
      );
    });
  });

  describe('when de duration is less than ten seconds', () => {
    it('pads the minutes with a "0"', () => {
      expect(renderDuration(3 * second)).toEqual(expect.stringMatching(/^0[0-9]:0[0-9]$/));
    });

    it('pads the hours with two "0"', () => {
      expect(renderDuration(3 * second)).toEqual(expect.stringMatching(/^0{2}/));
    });
  });

  describe('when de duration is negative', () => {
    it('pads the minutes with a "0" and adds a "-" at the begining', () => {
      expect(renderDuration(-3 * minute + 2 * second)).toEqual('-02:58');
    });
  });
});
