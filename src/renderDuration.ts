function pad(n: number): string {
  let s = Math.abs(n).toFixed(0);
  while (s.length < 2) s = `0${s}`;
  return s;
}

export const second = 1000;
export const minute = second * 60;

export function renderDuration(duration: number) {
  const minutes = ~~(duration / minute);
  const seconds = ~~((duration % minute) / second);
  const sg = duration < 0 ? '-' : '';

  return `${sg}${pad(minutes)}:${pad(seconds)}`;
}
