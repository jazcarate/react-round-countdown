export type Time = number; // Epoch

export type ExtendedTime = Time | string | Date;

export function toTime(ed: ExtendedTime): Time {
  if (!ed) {
    throw 'Needed a number, date or String';
  }
  return new Date(ed).getTime();
}
